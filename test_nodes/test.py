#!/usr/bin/env python
from cezeri_lib.cezeri import Robot
import rospy
import math

gain = 0.01
gain_rotate = 0.005
speed = 2.0 # speed along path - fixed right now

no_ring = 0
red_ring = 1
blue_ring = 2

current_best_ring = no_ring

robot = Robot("quadrotor")

#robot.move(1.0, 0.0, 0.0)
#robot.rotate(speed_yaw)
#robot.camera_height   image height
#robot.camera_width   image width
#robot.get_camera_data()   rgb values of image, returns one dim. list


raw_image = robot.get_camera_data()

# test code to scan a line through the center and print
# first check on our rgb values
center_i = robot.camera_height / 2
center_j = robot.camera_width / 2
for j in range(0, robot.camera_width):
    index = (center_i*robot.camera_width + j) * 3
    red = raw_image[index]
    green = raw_image[index+1]
    blue = raw_image[index+2]
    print(j,red,green,blue)

while robot.is_ok():
    raw_image = robot.get_camera_data()

    # Check each pixel
    # Define it as background if rgb sum is > 300 (to tune based on lights)
    # Otherwise it's red if red is the dominant pixel
    # Otherwise it's blue
    #
    # Count number of red and blue
    # Also calculate the mean position of all red and all blue

    count_red = 0
    count_blue = 0
    center_red_x = 0.0
    center_red_y = 0.0
    center_blue_x = 0.0
    center_blue_y = 0.0

    for i in range(0, robot.camera_height):
        for j in range(0, robot.camera_width):
            index = (i*robot.camera_width + j) * 3
            red = raw_image[index]
            green = raw_image[index+1]
            blue = raw_image[index+2]

            x = float(center_j - j)
            y = float(center_i - i)

            if red + green + blue < 300: # dumb approximation right now - to tune based on final lighting
                if red > blue and red > green:
                    count_red += 1
                    center_red_x += x # will take average
                    center_red_y += y # will take average
                else:
                    count_blue += 1
                    center_blue_x += x # will take average
                    center_blue_y += y # will take average

    if count_red > 0:
        center_red_x /= float(count_red)
        center_red_y /= float(count_red)
    if count_blue > 0:
        center_blue_x /= float(count_blue)
        center_blue_y /= float(count_blue)

  #  print(count_red, 'center_red', center_red_x, center_red_y)
  #  print(count_blue, 'center_blue', center_red_x, center_red_y)

    if count_red > 0 and count_red > count_blue:
        speed_x = center_red_x * gain
        speed_y = center_red_y * gain
        if current_best_ring != red_ring:
            current_best_ring = red_ring
            print("Switched to RED!")
    elif count_blue > 0:
        speed_x = center_blue_x * gain
        speed_y = center_blue_y * gain
        if current_best_ring != blue_ring:
            current_best_ring = blue_ring
            print("Switched to BLUE!")
    robot.move(speed, speed_x, speed_y)
    robot.rotate(speed_x * gain_rotate / gain) # generalize to pitch too

#while robot.is_ok():
#    raw_image = robot.get_camera_data()
#
#    image = []
#    for i in range(0, robot.camera_height):
#        image_row = []
#        for j in range(0, robot.camera_width):
#            image_row.append(raw_image[(i*3)*robot.camera_width + (j*3)])
#
#       image.append(image_row)
    

