import rospy
import math
import time
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32, String
from std_srvs.srv import Trigger
from gazebo_msgs.srv import GetModelState
from quadrotor_description.srv import LaserSensor, CameraSensor, DroneSensors
from cezeri_game_controller.grid_map import Grid, Zone, zone_size, grid_legth, grid_width
from cezeri_game_controller.srv import InitRobot
from cezeri_game_controller.msg import UpdateRobot

class Robot(object):
    def __init__(self, robot_name, keyboard = False):
        self. keyboard = keyboard
        rospy.init_node("cezeri_controller")

        self.__scenario = 0
        self.destination = []

        self.map = Grid(zone_size, grid_legth, grid_width)

        self._init_game_controller()
        self._init_drone_sensors()
        self._init_robot_control()

        self.slow_speed = 1.0
        self.medium_speed = 2.0
        self.fast_speed = 3.0

    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_init", 3)

            self.robot_init = rospy.ServiceProxy('/robot_init', InitRobot)

            resp = self.robot_init(self.__scenario)
            self.status = resp.message

            self.robot_update_sub = rospy.Subscriber('/robot_update', UpdateRobot, self.update)
            
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"
            rospy.logerr("Service call failed:")

    def update(self, msg):
        self.destination = msg.destination

    @property
    def scenario(self):
        return self.__scenario

    @scenario.setter
    def scenario(self, value):
        resp = self.robot_init(value)
        self.status = resp.message
        self.__scenario = value

    
    def _init_drone_sensors(self):
        try:
            rospy.wait_for_service("/quadrotor/quadrotor/get_sensor_data", 3)

            self.drone_sensors = rospy.ServiceProxy('/quadrotor/quadrotor/get_sensor_data', DroneSensors)
            self.sensor_data = self.drone_sensors()

            rospy.wait_for_service("/gazebo/get_model_state", 3)
            self.get_robot_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed:")

    def _init_robot_control(self):
        self.quadrotor_cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=10)

        self.twist_cmd = Twist()
        self.twist_cmd.linear.x = 0
        self.twist_cmd.linear.y = 0
        self.twist_cmd.linear.z = 0
        self.twist_cmd.angular.x = 0
        self.twist_cmd.angular.y = 0
        self.twist_cmd.angular.z = 0

        self.key_down_sub = rospy.Subscriber("/keyboard_down", String, self.key_down)
        self.key_up_sub = rospy.Subscriber("/keyboard_up", String, self.key_up)
    
    
    def key_down(self, msg):
        if self.keyboard:
            x = 0.2
            xyz = 2
            rotate = 2
            max_xy_speed = 3.0
            max_vertical_speed = 5.0

            messages = msg.data.split(',')
            for a in messages:
                if a == "w":
                    self.twist_cmd.linear.x += x
                    if self.twist_cmd.linear.x > max_xy_speed:
                        self.twist_cmd.linear.x = max_xy_speed
                elif a == "s":
                    self.twist_cmd.linear.x -= x
                    if self.twist_cmd.linear.x < -max_xy_speed:
                        self.twist_cmd.linear.x = -max_xy_speed
                elif a == "d":
                    self.twist_cmd.linear.y -= xyz
                    if self.twist_cmd.linear.y < -max_xy_speed:
                        self.twist_cmd.linear.y = -max_xy_speed
                elif a == "a":
                    self.twist_cmd.linear.y += xyz
                    if self.twist_cmd.linear.y > max_xy_speed:
                        self.twist_cmd.linear.y = max_xy_speed
                elif a == "p":
                    self.twist_cmd.linear.z -= xyz
                    if self.twist_cmd.linear.z < -max_vertical_speed:
                        self.twist_cmd.linear.z = -max_vertical_speed
                elif a == "o":
                    self.twist_cmd.linear.z += xyz
                    if self.twist_cmd.linear.z > max_vertical_speed:
                        self.twist_cmd.linear.z = max_vertical_speed
                elif a == "e":
                    if self.twist_cmd.linear.x >= 0:
                        self.twist_cmd.linear.x -= x
                    self.twist_cmd.angular.z -= rotate
                elif a == "q":
                    if self.twist_cmd.linear.x >= 0:
                        self.twist_cmd.linear.x -= x
                    self.twist_cmd.angular.z += rotate
                elif a == " ":
                    self.twist_cmd.linear.x = 0
                    self.twist_cmd.linear.y = 0
                    self.twist_cmd.linear.z = 0
                    self.twist_cmd.angular.z = 0

    def key_up(self, msg):
        if self.keyboard:
            messages = msg.data.split(',')
            for a in messages:
                if a == "d":
                    self.twist_cmd.linear.y = 0
                elif a == "a":
                    self.twist_cmd.linear.y = 0
                elif a == "p":
                    self.twist_cmd.linear.z = 0
                elif a == "o":
                    self.twist_cmd.linear.z = 0
                elif a == "e":
                    self.twist_cmd.angular.z = 0
                elif a == "q":
                    self.twist_cmd.angular.z = 0
                elif a == " ":
                    self.twist_cmd.linear.x = 0
                    self.twist_cmd.linear.y = 0
                    self.twist_cmd.linear.z = 0
                    self.twist_cmd.angular.z = 0
            return

    def move(self, speed_x, speed_y, speed_z):
        self.twist_cmd.linear.x = speed_x
        self.twist_cmd.linear.y = speed_y
        self.twist_cmd.linear.z = speed_z
        self.quadrotor_cmd_vel.publish(self.twist_cmd)

    def rotate(self, speed_yaw):
        self.twist_cmd.angular.z = speed_yaw
        self.quadrotor_cmd_vel.publish(self.twist_cmd)
        
    def stop(self):
        self.move(0, 0, 0)
        self.rotate(0)

    def move_forward(self, speed):
        self.move(speed, 0, 0)
    
    def move_backward(self, speed):
        self.move(-speed, 0, 0)

    def move_right(self, speed):
        self.move(0, -speed, 0)

    def move_left(self, speed):
        self.move(0, speed,0)

    def move_up(self, speed):
        self.move(0, 0, speed)

    def move_down(self, speed):
        self.move(0, 0, -speed)

    def rotate_right(self, speed):
        self.rotate(-3.14*speed)

    def rotate_left(self, speed):
        self.rotate(3.14*speed)

    def get_drone_sensors(self):
        self.sensor_data = self.drone_sensors()

    @property
    def gnss(self):
        class Gnss:
            def __init__(self, latitude, longitude, altitude, error, mix):
                self.latitude = latitude
                self.longitude = longitude
                self.altitude = altitude
                self.error = error
                self.mix = mix

            def __str__(self):
                message = {
                    "latitude": self.latitude,
                    "longitude": self.longitude,
                    "altitude": self.altitude,
                    "error": self.error,
                    "mix": self.mix,
                }
                return str(message)

        gnss = Gnss(
            self.sensor_data.gnss.latitude,
            self.sensor_data.gnss.longitude,
            self.sensor_data.gnss.altitude,
            self.sensor_data.gnss_status,
            self.sensor_data.gnss_mix_status,
        )
        return gnss
    
    @property
    def barometer(self):
        class Barometer:
            def __init__(self, altitude, pressure, qnh, error):
                self.altitude = altitude
                self.pressure = pressure
                self.qnh = qnh
                self.error = error

            def __str__(self):
                message = {
                    "altitude": self.altitude,
                    "pressure": self.pressure,
                    "qnh": self.qnh,
                    "error": self.error,
                }
                return str(message)
    
        barometer = Barometer(
            self.sensor_data.altitude,
            self.sensor_data.pressure,
            self.sensor_data.qnh,
            self.sensor_data.barometer_status,
        )
        return barometer

    @property
    def radar(self):
        class Radar:
            def __init__(self, radar, error):
                self.range = radar.ranges[0]
                self.error = error

            def __str__(self):
                message = {
                    "range": self.range,
                    "error": self.error,
                }
                return str(message)
    
        radar = Radar(
            self.sensor_data.radar,
            self.sensor_data.radar_status,
        )
        return radar


    @property
    def imu(self):
        class Imu:
            def __init__(self, imu, error, noise):
                self.orientation = imu.orientation
                self.angular_velocity = imu.angular_velocity
                self.linear_acceleration = imu.linear_acceleration
                self.error = error
                self.noise = noise

            def __str__(self):
                message = {
                    "orientation": self.orientation,
                    "angular_velocity": self.angular_velocity,
                    "linear_acceleration": self.linear_acceleration,
                    "error": self.error,
                    "noise": self.noise,
                }
                return str(message)
    
        imu = Imu(
            self.sensor_data.imu,
            self.sensor_data.imu_status,
            self.sensor_data.imu_noise,
        )
        return imu


    @property
    def lidar(self):
        class Lidar:
            def __init__(self, lidar, error):
                self.lidar = lidar.ranges[0]
                self.error = error

            def __str__(self):
                message = {
                    "lidar": self.lidar,
                    "error": self.error,
                }
                return str(message)
    
        lidar = Lidar(
            self.sensor_data.lidar,
            self.sensor_data.radar_status,
        )
        return lidar


    @property
    def magnetometer(self):
        class Magnetometer:
            def __init__(self, magnetometer, error, noise):
                self.data = magnetometer
                self.error = error
                self.noise = noise

            def __str__(self):
                message = {
                    "data": self.data,
                    "error": self.error,
                    "noise": self.noise,
                }
                return str(message)
    
        magnetometer = Magnetometer(
            self.sensor_data.magnetometer,
            self.sensor_data.magnetometer_status,
            self.sensor_data.magnetometer_noise,
        )
        return magnetometer

    @property
    def battery(self):
        class Battery:
            def __init__(self, battery, error, overheat):
                self.data = battery
                self.error = error
                self.overheat = overheat

            def __str__(self):
                message = {
                    "data": self.data,
                    "error": self.error,
                    "overheat": self.overheat,
                }
                return str(message)
    
        battery = Battery(
            self.sensor_data.battery,
            self.sensor_data.battery_status,
            self.sensor_data.battery_heat,
        )
        return battery

    @property
    def motor(self):
        class Motor:
            def __init__(self, motor, error, overheat):
                self.data = motor
                self.error = error
                self.overheat = overheat

            def __str__(self):
                message = {
                    "data": self.data,
                    "error": self.error,
                    "overheat": self.overheat,
                }
                return str(message)
    
        motor = Motor(
            self.sensor_data.motor,
            self.sensor_data.motor_status,
            self.sensor_data.motor_heat,
        )
        return motor


    @property
    def position(self):
        class Position:
            def __init__(self, data):
                self.x = data.pose.position.x
                self.y = data.pose.position.y
                self.z = data.pose.position.z

                roll, pitch, self.yaw = self.euler_from_quaternion(
                    data.pose.orientation.x,
                    data.pose.orientation.y,
                    data.pose.orientation.z,
                    data.pose.orientation.w,
                )

            def __str__(self):
                message = {
                    "x": self.x,
                    "y": self.y,
                    "z": self.z,
                    "yaw": self.yaw,
                }
                return str(message)

    
            def euler_from_quaternion(self, x, y, z, w):
                t0 = +2.0 * (w * x + y * z)
                t1 = +1.0 - 2.0 * (x * x + y * y)
                roll_x = math.atan2(t0, t1)
            
                t2 = +2.0 * (w * y - z * x)
                t2 = +1.0 if t2 > +1.0 else t2
                t2 = -1.0 if t2 < -1.0 else t2
                pitch_y = math.asin(t2)
            
                t3 = +2.0 * (w * z + x * y)
                t4 = +1.0 - 2.0 * (y * y + z * z)
                yaw_z = math.atan2(t3, t4)
            
                return roll_x, pitch_y, yaw_z # in radians
    
        position = Position(
            self.get_robot_state("quadrotor", "")
        )
        return position


    def is_ok(self):
        if not rospy.is_shutdown() and self.status != "no_robot":
            if not self.status == "free_roam":
                pass

            if self.status == "Stop":
                return False

            rospy.sleep(0.05)
            self.get_drone_sensors()
            return True
        else:
            return False